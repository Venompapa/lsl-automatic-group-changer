// Automatic Group Changer
// Made by Kurtis Anatine
//  
// Get the latest copy from https://bitbucket.org/Vargink/lsl-automatic-group-changer/
// Please fork if you feel like it!
//
// Description: 
// This script with the use of rlv will detect the group of the area you have entered 
// and then change your active group to it if you have that group.
default
{
    changed(integer change)
    {
        if (change & CHANGED_REGION)
        {
            string URL_GROUP = "http://world.secondlife.com/group/";
            string groupUID = llList2String(llGetParcelDetails(llGetPos(), [PARCEL_DETAILS_GROUP]), 0);
            llHTTPRequest(URL_GROUP + groupUID + "?lang=en-US", [HTTP_METHOD, "GET"], "");
        }
    }
    http_response(key request_id, integer status, list metadata, string body)
    {
        string prefixStart = "<title>";
        string prefixEnd = "</title>";
        integer indexStart = llSubStringIndex(body, prefixStart);
        integer indexEnd = llSubStringIndex(body, prefixEnd);
        if((indexStart != -1) && (indexEnd != -1))
        {
            indexStart += llStringLength(prefixStart);
            indexEnd -= 1;
            string groupName = llGetSubString(body, indexStart, indexEnd);
            llOwnerSay("@setgroup:"+ groupName + "=force");
        }
    }
}