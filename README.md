# Automatic Group Changer #

This is a script for the Automatic Group Changer. This script with the help of rlv will automatically change your group to the parcel / sims group if it is in your group list.
Please feel free to fork and do whatever you like with it!

### Features ###

* Changes your group when you arrive at a sim / parcel. Saves you having to do it etc.

### Who do I talk to? ###

* [Kurtis Anatine](https://my.secondlife.com/kurtis.anatine)